<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class UserForm extends Model
{
    public $first_name;
    public $last_name;
    public $phone;

    //private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['first_name', 'last_name', 'phone'], 'required'],
            [['first_name', 'last_name'], 'filter', 'filter' => 'trim'],
            ['first_name', 'string', 'min' => 2, 'max' => 21],
            ['last_name', 'string', 'min' => 2, 'max' => 255],
            ['phone', 'integer'],
        ];
    }

}
