<?php

namespace app\models;

use app\models\Address;

class User extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return 'user';
    }

    public function Address() {
        return $this->hasOne(Address::class, ['user_id' => 'id']);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    public static function findIdentity($id)
    {
        return null;

    }
}
