<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class AddressForm extends Model
{
    public $city;
    public $street;
    public $house_number;

    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['city', 'street', 'house_number'], 'required'],
            [['city', '$street'], 'filter', 'filter' => 'trim'],
            ['house_number', 'number'],
        ];
    }

}
