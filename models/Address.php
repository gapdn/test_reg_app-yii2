<?php

namespace app\models;

use Yii;
use app\models\User;

/**
 * This is the model class for table "address".
 *
 * @property int $id
 * @property int $user_id
 * @property string $city
 * @property string $street
 * @property int $house_number
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'house_number'], 'integer'],
            [['city', 'street'], 'string', 'max' => 255],
        ];
    }
    
    public function User() {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'city' => 'City',
            'street' => 'Street',
            'house_number' => 'House Number',
        ];
    }
}
