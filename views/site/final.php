<?php

/* @var $this yii\web\View */

$this->title = 'Final';


?>
<div class="site-index">

    <div class="jumbotron">
        <?php if(!empty($id)): ?>
            <h1>Congratulations!</h1>

            <p class="lead">You have successfully registered!</p>

            <p><strong>Your feedback id: <?= $id ?></strong></p>
        
        <?php else: ?>
            
            <h1>Oops!</h1>

            <p class="lead">Something wrong while sending data! Try again</p>
            
        <?php endif; ?>
    </div>
</div>

<?php Yii::$app->session->destroy();