<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\AddressForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


$this->title = 'address data';
$this->params['breadcrumbs'][] = ['label' => 'user data', 'url' => ['/site/user']];// $this->title;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'address-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

        <?= $form->field($model, 'city')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'street')->textInput() ?>

        <?= $form->field($model, 'house_number')->textInput() ?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::a('prev', ['user'], ['class'=>'btn btn-primary'])?>
                <?= Html::submitButton('next', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>

</div>
