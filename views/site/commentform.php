<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\UserForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'comment';
$this->params['breadcrumbs'][] = ['label' => 'user data', 'url' => ['/site/user']];
$this->params['breadcrumbs'][] = ['label' => 'address data', 'url' => ['/site/address']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Leave your comment:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'comment-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

        <?= $form->field($model, 'comment')->textarea() ?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?php echo Html::a('prev', ['address'], ['class'=>'btn btn-primary'])?>
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>

</div>
