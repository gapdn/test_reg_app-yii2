<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\UserForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'user data';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'user-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

        <?= $form->field($model, 'first_name')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'last_name')->textInput() ?>

        <?= $form->field($model, 'phone')->textInput() ?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('next', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                <?php //echo Html::a('prev', ['address'], ['class'=>'btn btn-primary'])?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>

</div>
