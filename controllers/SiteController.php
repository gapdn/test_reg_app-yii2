<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\UserForm;
use app\models\AddressForm;
use app\models\CommentForm;

class SiteController extends Controller
{
        
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $cookies = Yii::$app->request->cookies;
        if (empty($cookies['curr_page'])) {
            $curr_page = '1';
        } else {
            $curr_page = $cookies['curr_page']->value;
        }
        
        $url = '';
        switch ($curr_page) {
            case '1':
                $url = '/site/user';
                break;
            case '2':
                $url = '/site/address';
                break;
            case '3':
                $url = '/site/comment';
                break;
            default :
                $url = '/site/user';
        }
        return $this->render('index', [
            'url' => $url,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionUser()
    {
        $session = Yii::$app->session;
        $model = new UserForm();
        if ($session->has('firstname')) {
            $model->first_name = $session['firstname'];
            $model->last_name = $session['lastname'];
            $model->phone = $session['phone'];
        }
        if ($model->load(Yii::$app->request->post())) {
            $session->set('firstname', $model->first_name);
            $session->set('lastname', $model->last_name);
            $session->set('phone', $model->phone);
            
            return $this->redirect('address');
        }

        return $this->render('userform', [
            'model' => $model,
        ]);
    }
    
    public function actionAddress() {
        $cookie = Yii::$app->response->cookies;
        $cookie->add(new \yii\web\Cookie([
            'name' => 'curr_page',
            'value' => '2',
        ]));
        $session = Yii::$app->session;
        $model = new AddressForm();
        if ($session->has('city')) {
            $model->city = $session['city'];
            $model->street = $session['street'];
            $model->house_number = $session['house_number'];
        }
        if ($model->load(Yii::$app->request->post())) {
           $session->set('city', $model->city);
           $session->set('street', $model->street);
           $session->set('house_number', $model->house_number);
           

           return $this->redirect('comment');
        }
         
        return $this->render('addressform', [
            'model' => $model,
        ]);
    }
    
    public function actionComment() {
        
        $session = Yii::$app->session;
        $cookie = Yii::$app->response->cookies;
        $cookie->add(new \yii\web\Cookie([
            'name' => 'curr_page',
            'value' => '3',
        ]));
        $model = new CommentForm();
        if (empty($session['firstname'])) {
                $this->redirect('/site/index');
                $cookie->add(new \yii\web\Cookie([
                    'name' => 'curr_page',
                    'value' => '1',
                ]));
                return;
            }
        if ($model->load(Yii::$app->request->post())) {
            
            $session = Yii::$app->session;
            
            
            
            $user = new \app\models\User();
            $user->first_name = $session['firstname'];
            $user->last_name = $session['lastname'];
            $user->phone = $session['phone'];
            if ($user->save()) {
                $user_address = new \app\models\Address();
                $user_address->city = $session['city'];
                $user_address->street = $session['street'];
                $user_address->house_number = $session['house_number'];
                $user_address->user_id = $user->id;
                $user_address->save();
                
            }
            
            $full_address = $session['city'] . ', ' . $session['street'] . ' ' . $session['house_number'];
            
            $feedBackDataId = $this->getFeedbackId($user->id, $full_address, !empty($model->comment) ? $model->comment : '');
            
            $session->set('feedbackDataId', $feedBackDataId);
            $user->feedBackDataId = $feedBackDataId;
            $user->save();
            
            Yii::$app->getSession()->setFlash('success', 'Congrats!!!');
            
            
            $this->redirect('final');
            return;
        }
        return $this->render('commentform', [
            'model' => $model,
        ]);
    }
    
    public function actionFinal() {
        
        $session = Yii::$app->session;
        $cookie = Yii::$app->response->cookies;
        $cookie->add(new \yii\web\Cookie([
            'name' => 'curr_page',
            'value' => '1',
        ]));
        
        $feedBackDataId = $session['feedbackDataId'];
        $session->destroy();
        
        return $this->render('final', [
            'id' => $feedBackDataId,
        ]);
    }

    private function getFeedbackId($client_id, $address, $comment = '') {
        
        $data = [
                'client_id' => $client_id, 
                'address' => $address, 
                'comment' => $comment, 
            ];
            
        $json_content = json_encode($data);

        $url = Yii::$app->params['feedbackUrl'];

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array('Content-type: application/json; charset=utf8')
        );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_content);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 2);
        $result = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        if ($http_code != '200') {
            Yii::error('Test feedback error: '.$result);
            $this->redirect('final');
            return;
        }

        //var_dump($result);die;
        $result_data = json_decode($result);
        
        return $result_data->feedbackDataId;
    }
}
